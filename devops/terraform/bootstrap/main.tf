locals {
  environment = "dev"
  aws_region  = "us-east-1"
  project     = "rails"
}

module "terraform_state" {
  source      = "../modules/aws-s3-terraform-backend"
  environment = local.environment
  project     = local.project
  aws_region  = local.aws_region
}
