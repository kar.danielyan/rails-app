output "s3_bucket_arn" {
  value       = module.terraform_state.s3_bucket_arn
  description = "The ARN of the S3 bucket"
}

output "dynamodb_table_arn" {
  value       = module.terraform_state.dynamodb_table_arn
  description = "The ARN of the DynamoDB table"
}
