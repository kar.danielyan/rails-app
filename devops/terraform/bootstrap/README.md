# Environment bootstrapper

This folder contains [Terraform](https://www.terraform.io/) configuration that creates an [S3](https://aws.amazon.com/s3/) bucket,  <br /> 
and [DynamoDB](https://aws.amazon.com/dynamodb/) table. <br />
The S3 bucket and DynamoDB table can be used as a [remote backend for Terraform](https://www.terraform.io/docs/backends/).

## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | > 1.0, < 1.1.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 3.73.0 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_terraform_state"></a> [terraform\_state](#module\_terraform\_state) | ../modules/aws-s3-terraform-backend | n/a |

## Inputs

For the Environment configuration we can edit locals in the `main.tf` file.
```hcl
locals {
  environment = "dev"
  aws_region  = "us-east-1"
}
```

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_dynamodb_table_arn"></a> [dynamodb\_table\_arn](#output\_dynamodb\_table\_arn) | The ARN of the DynamoDB table |
| <a name="output_s3_bucket_arn"></a> [s3\_bucket\_arn](#output\_s3\_bucket\_arn) | The ARN of the S3 bucket |

## Quick start

Configure
your [AWS access keys](http://docs.aws.amazon.com/general/latest/gr/aws-sec-cred-types.html#access-keys-and-secret-access-keys)
as environment variables:

```bash
export AWS_ACCESS_KEY_ID=(your access key id)
export AWS_SECRET_ACCESS_KEY=(your secret access key)
```

Deploy the code:

```
terraform init
terraform apply
```

Clean up when you're done:
```
terraform destroy
```
