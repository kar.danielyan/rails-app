output "s3_bucket_arn" {
  value       = aws_s3_bucket.s3.arn
  description = "The ARN of the S3 bucket"
}

output "dynamodb_table_arn" {
  value       = aws_dynamodb_table.dynamodb.arn
  description = "The ARN of the DynamoDB table"
}
