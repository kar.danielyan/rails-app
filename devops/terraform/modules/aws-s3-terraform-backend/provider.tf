terraform {
  required_version = "> 1.0, < 1.1.0"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.73.0"
    }
  }
}

provider "aws" {
  region = var.aws_region
  default_tags {
    tags = merge(var.tags, {
      Terraformed = "Please do not edit manually"
      Environment = var.environment
      Project = var.project
    })
  }
}
