variable "project" {
  description = "Project name"
  type        = string
}

variable "environment" {
  description = "Environment Type"
  type        = string
}

variable "aws_region" {
  description = "AWS region for all resources."
  type        = string
}

variable "tags" {
  description = "Tags to set"
  type        = map(string)
  default = {}
}
