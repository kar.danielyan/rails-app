terraform {
  backend "s3" {

    bucket         = "rails-dev-terraform-bucket"
    key            = "terraform.tfstate"
    region         = "us-east-1"
    dynamodb_table = "rails-dev-terraform-table"
    encrypt        = true

  }
}
