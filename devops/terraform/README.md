<!-- BEGIN_TF_DOCS -->
# Environment Deployment

This folder contains [Terraform](https://www.terraform.io/) configuration that creates the environment.
The configuration uses [Official Terraform modules](https://registry.terraform.io/namespaces/terraform-aws-modules).

## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | > 1.0, < 1.1.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 3.73.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 3.73.0 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_app_ec2"></a> [app\_ec2](#module\_app\_ec2) | terraform-aws-modules/ec2-instance/aws | ~> 3.4 |
| <a name="module_db_ec2"></a> [db\_ec2](#module\_db\_ec2) | terraform-aws-modules/ec2-instance/aws | ~> 3.4 |
| <a name="module_http_sg"></a> [http\_sg](#module\_http\_sg) | terraform-aws-modules/security-group/aws | ~> 4.0 |
| <a name="module_mysql_sg"></a> [mysql\_sg](#module\_mysql\_sg) | terraform-aws-modules/security-group/aws | ~> 4.0 |
| <a name="module_ssh_sg"></a> [ssh\_sg](#module\_ssh\_sg) | terraform-aws-modules/security-group/aws | ~> 4.0 |
| <a name="module_vpc"></a> [vpc](#module\_vpc) | terraform-aws-modules/vpc/aws | ~> 3.0 |

## Resources

| Name | Type |
|------|------|
| [aws_key_pair.ssh_key](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/key_pair) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_ami"></a> [ami](#input\_ami) | ID of AMI to use for the instance | `string` | `"ami-04505e74c0741db8d"` | no |
| <a name="input_associate_public_ip_address"></a> [associate\_public\_ip\_address](#input\_associate\_public\_ip\_address) | Whether to associate a public IP address with an instance in a VPC | `bool` | `true` | no |
| <a name="input_cidr"></a> [cidr](#input\_cidr) | The CIDR block for the VPC. Default value is a valid CIDR, but not acceptable by AWS and should be overridden | `string` | `"10.99.0.0/18"` | no |
| <a name="input_instance_type"></a> [instance\_type](#input\_instance\_type) | The type of instance to start | `string` | `"t2.micro"` | no |
| <a name="input_private_subnets"></a> [private\_subnets](#input\_private\_subnets) | A list of private subnets inside the VPC | `list(string)` | <pre>[<br>  "10.99.3.0/24",<br>  "10.99.4.0/24",<br>  "10.99.5.0/24"<br>]</pre> | no |
| <a name="input_public_subnets"></a> [public\_subnets](#input\_public\_subnets) | A list of public subnets inside the VPC | `list(string)` | <pre>[<br>  "10.99.0.0/24",<br>  "10.99.1.0/24",<br>  "10.99.2.0/24"<br>]</pre> | no |
| <a name="input_timeouts"></a> [timeouts](#input\_timeouts) | Define maximum timeout for creating, updating, and deleting EC2 instance resources | `map(string)` | <pre>{<br>  "Delete": "20m",<br>  "Update": "20m",<br>  "create": "20m"<br>}</pre> | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_app_ec2_public_ip"></a> [app\_ec2\_public\_ip](#output\_app\_ec2\_public\_ip) | The public IP address assigned to the APP instance. |
| <a name="output_db_ec2_private_ip"></a> [db\_ec2\_private\_ip](#output\_db\_ec2\_private\_ip) | The private IP address assigned to the DB instance. |
| <a name="output_db_ec2_public_ip"></a> [db\_ec2\_public\_ip](#output\_db\_ec2\_public\_ip) | The public IP address assigned to the DB instance. |

## Quick start

For the execution of Terraform files, we can use either personal credentials or we can generate a new one special for Terraform. 
Before provisioning the environment we should manually execute bootstrap configuration.
Bootstrap configuration will create an [S3](https://aws.amazon.com/s3/) bucket and [DynamoDB](https://aws.amazon.com/dynamodb/) table in an
[Amazon Web Services (AWS) account](http://aws.amazon.com/).

The bucket uses for Terraform remote backend. It keeps the state file safe.
Configure your [AWS access keys](http://docs.aws.amazon.com/general/latest/gr/aws-sec-cred-types.html#access-keys-and-secret-access-keys)
as environment variables.
To export AWS credentials, we have to run the following commands:

```bash
export AWS_ACCESS_KEY_ID=(your access key id)
export AWS_SECRET_ACCESS_KEY=(your secret access key)
```

Deploy the code:

```
terraform init
terraform apply
```

Clean up when you're done:
```
terraform destroy
```


<!-- END_TF_DOCS -->