output "app_ec2_public_ip" {
  description = "The public IP address assigned to the APP instance."
  value       = module.app_ec2.public_ip
}

output "db_ec2_public_ip" {
  description = "The public IP address assigned to the DB instance."
  value       = module.db_ec2.public_ip
}

output "db_ec2_private_ip" {
  description = "The private IP address assigned to the DB instance."
  value       = module.db_ec2.private_ip
}