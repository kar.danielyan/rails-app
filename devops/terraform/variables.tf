variable "timeouts" {
  description = "Define maximum timeout for creating, updating, and deleting EC2 instance resources"
  type        = map(string)
  default     = {
    create = "20m"
    Update = "20m"
    Delete = "20m"
  }
}

variable "ami" {
  description = "ID of AMI to use for the instance"
  type        = string
  default     = "ami-04505e74c0741db8d"
}

variable "instance_type" {
  description = "The type of instance to start"
  type        = string
  default     = "t2.micro"
}

variable "associate_public_ip_address" {
  description = "Whether to associate a public IP address with an instance in a VPC"
  type        = bool
  default     = true
}

variable "cidr" {
  description = "The CIDR block for the VPC. Default value is a valid CIDR, but not acceptable by AWS and should be overridden"
  type        = string
  default     = "10.99.0.0/18"
}
variable "public_subnets" {
  description = "A list of public subnets inside the VPC"
  type        = list(string)
  default     = ["10.99.0.0/24", "10.99.1.0/24", "10.99.2.0/24"]
}

variable "private_subnets" {
  description = "A list of private subnets inside the VPC"
  type        = list(string)
  default     = ["10.99.3.0/24", "10.99.4.0/24", "10.99.5.0/24"]
}
