locals {
  app_name   = "rails-app"
  db_name    = "rails-db"
  region     = "us-east-1"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDg2duc4SiR7k5KW6Kcn3KAbx8oghF7Bx72cmJ9CBvVV/8ZGotSYrAnHJm0y5/fiFF8LDvIVt7RqAa2JB0qcjEXMBy/WZvpLQD50TZYNbTd13UIfPjY0C2CkQfLGEKW7s7Z8jqFqOL81VrtDyLw4WbQayFlEqJJFLEDyQCA1wkJaqM9QWtDm1v/VisnbDq1PPbqO31iWRdF9h4e4VBzmP03cfDb3QU5ugfFpIRq6H+Qg/U5V+l0PBwjg0ggUYqDSMp7Gp/rWWB8+wHL9NEFht0YwKB6aCscBQttlx2NujewhResy7Vg5a2rnXK84rv8c9CKYePHcYHdrtopRqLYJ8FPryLXkEGt3XKcZj1SiXc5ClbU1ecxGn6ZKn25/ekvgCZgGJ6ZZAYwxHhQ6YT21ayVbo4Fw0HyJ/4T6fAwoHHlPNNqgExLs7J8hm1s+oOFdxCxCqcvOzb/TzBCUG1WDDz6zDHxY1q9xf07KmGNdobvY0tAbCy+IIKHr00sKPglog0= Ansible"

  tags = {
    Owner       = "user"
    Environment = "dev"
    Project     = "rails-app"
  }
}

# Networking
module "vpc" {
  source          = "terraform-aws-modules/vpc/aws"
  version         = "~> 3.0"
  name            = local.app_name
  cidr            = var.cidr
  azs             = ["${local.region}a", "${local.region}b", "${local.region}c"]
  public_subnets  = var.public_subnets
  private_subnets = var.private_subnets
  tags            = local.tags
}

module "ssh_sg" {
  source              = "terraform-aws-modules/security-group/aws"
  version             = "~> 4.0"
  name                = "${local.app_name}-ssh"
  description         = "Security group for SSH to EC2 instance"
  vpc_id              = module.vpc.vpc_id
  ingress_cidr_blocks = ["0.0.0.0/0"]
  ingress_rules       = ["ssh-tcp"]
  egress_rules        = ["all-all"]
  tags                = local.tags
}

module "http_sg" {
  source              = "terraform-aws-modules/security-group/aws"
  version             = "~> 4.0"
  name                = "${local.app_name}-http"
  description         = "Security group for HTTP and HTTPS to EC2 instance"
  vpc_id              = module.vpc.vpc_id
  ingress_cidr_blocks = ["0.0.0.0/0"]
  ingress_rules       = ["http-80-tcp", "https-443-tcp"]
  egress_rules        = ["all-all"]
  tags                = local.tags
}

module "mysql_sg" {
  source              = "terraform-aws-modules/security-group/aws"
  version             = "~> 4.0"
  name                = "${local.db_name}-mysql"
  description         = "Security group for MySQL on EC2 instance"
  vpc_id              = module.vpc.vpc_id
  ingress_cidr_blocks = var.public_subnets
  ingress_rules       = ["mysql-tcp"]
  egress_cidr_blocks  = var.public_subnets
  tags                = local.tags
}

# EC2 Modules
resource "aws_key_pair" "ssh_key" {
  public_key = local.public_key
}

module "app_ec2" {
  source                      = "terraform-aws-modules/ec2-instance/aws"
  version                     = "~> 3.4"
  name                        = local.app_name
  ami                         = var.ami
  instance_type               = var.instance_type
  availability_zone           = element(module.vpc.azs, 0)
  subnet_id                   = element(module.vpc.public_subnets, 0)
  vpc_security_group_ids      = [module.http_sg.security_group_id, module.ssh_sg.security_group_id]
  associate_public_ip_address = var.associate_public_ip_address
  key_name                    = aws_key_pair.ssh_key.key_name

  enable_volume_tags = false
  root_block_device  = [
    {
      encrypted   = true
      volume_type = "gp3"
      throughput  = 200
      volume_size = 20
      tags        = {
        Name = "app-root-block"
      }
    },
  ]

  tags     = local.tags
  timeouts = var.timeouts
}

module "db_ec2" {
  source                      = "terraform-aws-modules/ec2-instance/aws"
  version                     = "~> 3.4"
  name                        = local.db_name
  ami                         = var.ami
  instance_type               = var.instance_type
  availability_zone           = element(module.vpc.azs, 1)
  subnet_id                   = element(module.vpc.public_subnets, 1)
  vpc_security_group_ids      = [module.mysql_sg.security_group_id, module.ssh_sg.security_group_id]
  associate_public_ip_address = var.associate_public_ip_address
  key_name                    = aws_key_pair.ssh_key.key_name

  enable_volume_tags = false
  root_block_device  = [
    {
      encrypted   = true
      volume_type = "gp3"
      throughput  = 200
      volume_size = 20
      tags        = {
        Name = "db-root-block"
      }
    },
  ]

  tags     = local.tags
  timeouts = var.timeouts
}
