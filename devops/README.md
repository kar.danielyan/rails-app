The directory contains Terraform and Ansible code for provisioning, configuration, and deployment of the Rails application. 

For more information about Terraform modules and files please see the README file in the terraform directory.

Variables
--------------
There are mandatory variables that should be set in GitLab-Ci Settings as a file, which will be used as secure credentials:

```yaml
DB_DATABASE: '********'
DB_USERNAME: '********'
DB_PASSWORD: '********'
MASTER_KEY: '********'
```

The variables are used in the playbook.

```yaml
    app_dir: /var/www/rails-app
    passenger_server_name: "rails-app.local"
    passenger_app_root: "{{ app_dir }}/public"
    ruby_version: 2.6.6
    ruby_download_url: http://cache.ruby-lang.org/pub/ruby/2.6/ruby-2.6.6.tar.gz
    ruby_install_bundler: true
    ruby_install_from_source: true
    environment_config:
      RAILS_APP_DATABASE_HOST: "{{ DB_HOST }}"
      RAILS_APP_DATABASE_NAME: "{{ DB_DATABASE }}"
      RAILS_APP_DATABASE_USERNAME: "{{ DB_USERNAME }}"
      RAILS_APP_DATABASE_PASSWORD: "{{ DB_PASSWORD }}"
    apt_packages:
      - libmysqlclient-dev
      - build-essential
      - apt-transport-https
      - gcc
      - g++
      - make
      - nodejs
      - yarn
```

Dependencies
------------

The playbook uses Ansible collections and roles from Ansible Galaxy.
```yaml
collections:
  - community.mysql
  - community.general
roles:
  - weareinteractive.environment
  - geerlingguy.mysql
  - geerlingguy.ruby
  - geerlingguy.passenger

```

| Name | Source | Description                     |
|------|--------|---------------------------------|
| weareinteractive.environment | https://galaxy.ansible.com/weareinteractive/environment | Adds /etc/environment variables |
| geerlingguy.mysql | https://galaxy.ansible.com/geerlingguy/mysql | MySQL server for RHEL/CentOS and Debian/Ubuntu. |
| geerlingguy.ruby | https://galaxy.ansible.com/geerlingguy/ruby | Ruby installation for Linux. |
| geerlingguy.passenger | https://galaxy.ansible.com/geerlingguy/passenger | Passenger installation for Linux/UNIX. |

License
-------
Apache

Author Information
------------------

authors:
  - Karen Danielyan <kar.danielyan@gmail.com>
