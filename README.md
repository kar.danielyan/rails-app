# Environments and deployments
The repository contains Rails application and infrastructure code.

## Architecture

For the project created CI/CD Pipeline based on Gitlab-CI, Terraform and Ansible.
The pipeline has three stages:
  - Provisioning
  - Ansible Deploy
  - Destroy Env

| Stage          | Tool      | Description                                                                      |
|----------------|-----------|----------------------------------------------------------------------------------|
| Provisioning   | Terraform | The stage creates the environment and generates host.ini file for Ansible.       |
| Ansible Deploy | Ansible   | The stage configures the environment and deploys the application.                |
| Destroy Env    | Terraform | The stage destroys the environment manually or after the deletion of the branch. |

## Quick start

- [x] **Step 1: S3 bucket creation**

Create and configure your [AWS access keys](http://docs.aws.amazon.com/general/latest/gr/aws-sec-cred-types.html#access-keys-and-secret-access-keys)
as environment variables.
```bash
export AWS_ACCESS_KEY_ID=(your access key id)
export AWS_SECRET_ACCESS_KEY=(your secret access key)
```
```bash
cd devops/terraform/bootstrap
terraform init
terraform apply
```
- [x] **Step 2: Define all necessary variables in GitLab-CI**
```
In GitLab interface go to Settings -> CI/CD -> Variables -> Add Variables
```
| Type     | Key                   | Description                                                                                                                                   |
|----------|-----------------------|-----------------------------------------------------------------------------------------------------------------------------------------------|
| File     | ansible_vars          | Var file for Ansible. The file should contains values for: <br/> - DB_DATABASE: <br/> - DB_USERNAME: <br/> - DB_PASSWORD: <br/> - MASTER_KEY: |
| Variable | AWS_ACCESS_KEY_ID     | Your access key id.                                                                                                                           |
| Variable | AWS_SECRET_ACCESS_KEY | Your secret access key.                                                                                                                       |
| File     | ssh_key               | SSH Private key for Ansible.                                                                                                                  |

That's all,

The rest will do Gitlab, Terraform and Ansible.

Enjoy :-)

## Terraform

This folder contains [Terraform](https://www.terraform.io/) configuration that creates the environment.
The configuration uses [Official Terraform modules](https://registry.terraform.io/namespaces/terraform-aws-modules).

### Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | > 1.0, < 1.1.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 3.73.0 |

### Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 3.73.0 |

### Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_app_ec2"></a> [app\_ec2](#module\_app\_ec2) | terraform-aws-modules/ec2-instance/aws | ~> 3.4 |
| <a name="module_db_ec2"></a> [db\_ec2](#module\_db\_ec2) | terraform-aws-modules/ec2-instance/aws | ~> 3.4 |
| <a name="module_http_sg"></a> [http\_sg](#module\_http\_sg) | terraform-aws-modules/security-group/aws | ~> 4.0 |
| <a name="module_mysql_sg"></a> [mysql\_sg](#module\_mysql\_sg) | terraform-aws-modules/security-group/aws | ~> 4.0 |
| <a name="module_ssh_sg"></a> [ssh\_sg](#module\_ssh\_sg) | terraform-aws-modules/security-group/aws | ~> 4.0 |
| <a name="module_vpc"></a> [vpc](#module\_vpc) | terraform-aws-modules/vpc/aws | ~> 3.0 |

### Resources

| Name | Type |
|------|------|
| [aws_key_pair.ssh_key](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/key_pair) | resource |

### Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_ami"></a> [ami](#input\_ami) | ID of AMI to use for the instance | `string` | `"ami-04505e74c0741db8d"` | no |
| <a name="input_associate_public_ip_address"></a> [associate\_public\_ip\_address](#input\_associate\_public\_ip\_address) | Whether to associate a public IP address with an instance in a VPC | `bool` | `true` | no |
| <a name="input_cidr"></a> [cidr](#input\_cidr) | The CIDR block for the VPC. Default value is a valid CIDR, but not acceptable by AWS and should be overridden | `string` | `"10.99.0.0/18"` | no |
| <a name="input_instance_type"></a> [instance\_type](#input\_instance\_type) | The type of instance to start | `string` | `"t2.micro"` | no |
| <a name="input_private_subnets"></a> [private\_subnets](#input\_private\_subnets) | A list of private subnets inside the VPC | `list(string)` | <pre>[<br>  "10.99.3.0/24",<br>  "10.99.4.0/24",<br>  "10.99.5.0/24"<br>]</pre> | no |
| <a name="input_public_subnets"></a> [public\_subnets](#input\_public\_subnets) | A list of public subnets inside the VPC | `list(string)` | <pre>[<br>  "10.99.0.0/24",<br>  "10.99.1.0/24",<br>  "10.99.2.0/24"<br>]</pre> | no |
| <a name="input_timeouts"></a> [timeouts](#input\_timeouts) | Define maximum timeout for creating, updating, and deleting EC2 instance resources | `map(string)` | <pre>{<br>  "Delete": "20m",<br>  "Update": "20m",<br>  "create": "20m"<br>}</pre> | no |

### Outputs

| Name | Description |
|------|-------------|
| <a name="output_app_ec2_public_ip"></a> [app\_ec2\_public\_ip](#output\_app\_ec2\_public\_ip) | The public IP address assigned to the APP instance. |
| <a name="output_db_ec2_private_ip"></a> [db\_ec2\_private\_ip](#output\_db\_ec2\_private\_ip) | The private IP address assigned to the DB instance. |
| <a name="output_db_ec2_public_ip"></a> [db\_ec2\_public\_ip](#output\_db\_ec2\_public\_ip) | The public IP address assigned to the DB instance. |


## Ansible

The directory contains Terraform and Ansible code for provisioning, configuration, and deployment of the Rails application. 

For more information about Terraform modules and files please see the README file in to terraform directory.

### Variables
There are mandatory variables that should be set in GitLab-Ci Settings as a file, which will be used as secure credentials:

```yaml
DB_DATABASE: '********'
DB_USERNAME: '********'
DB_PASSWORD: '********'
MASTER_KEY: '********'
```

The variables are used in the playbook.

```yaml
    app_dir: /var/www/rails-app
    passenger_server_name: "rails-app.local"
    passenger_app_root: "{{ app_dir }}/public"
    ruby_version: 2.6.6
    ruby_download_url: http://cache.ruby-lang.org/pub/ruby/2.6/ruby-2.6.6.tar.gz
    ruby_install_bundler: true
    ruby_install_from_source: true
    environment_config:
      RAILS_APP_DATABASE_HOST: "{{ DB_HOST }}"
      RAILS_APP_DATABASE_NAME: "{{ DB_DATABASE }}"
      RAILS_APP_DATABASE_USERNAME: "{{ DB_USERNAME }}"
      RAILS_APP_DATABASE_PASSWORD: "{{ DB_PASSWORD }}"
    apt_packages:
      - libmysqlclient-dev
      - build-essential
      - apt-transport-https
      - gcc
      - g++
      - make
      - nodejs
      - yarn
```

### Dependencies

The playbook uses Ansible collections and roles from Ansible Galaxy.
```yaml
collections:
  - community.mysql
  - community.general
roles:
  - weareinteractive.environment
  - geerlingguy.mysql
  - geerlingguy.ruby
  - geerlingguy.passenger

```

| Name                         | Source                                                  | Description                                     |
|------------------------------|---------------------------------------------------------|-------------------------------------------------|
| weareinteractive.environment | https://galaxy.ansible.com/weareinteractive/environment | Adds /etc/environment variables                 |
| geerlingguy.mysql            | https://galaxy.ansible.com/geerlingguy/mysql            | MySQL server for RHEL/CentOS and Debian/Ubuntu. |
| geerlingguy.ruby             | https://galaxy.ansible.com/geerlingguy/ruby             | Ruby installation for Linux.                    |
| geerlingguy.passenger        | https://galaxy.ansible.com/geerlingguy/passenger        | Passenger installation for Linux/UNIX.          |

